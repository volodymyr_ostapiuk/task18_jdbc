-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema airport
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `airport` DEFAULT CHARACTER SET utf8 ;
USE `airport` ;

-- -----------------------------------------------------
-- Table `airport`.`plane_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`plane_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `airport`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `airport`.`plane`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`plane` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `chair_amount` INT NOT NULL,
  `plane_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_plane_plane_type_idx` (`plane_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_plane_plane_type`
    FOREIGN KEY (`plane_type_id`)
    REFERENCES `airport`.`plane_type_id` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `airport`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `airport`.`book` (
  `plane_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`plane_id`, `client_id`),
  INDEX `fk_book_plane_idx` (`plane_id` ASC) VISIBLE,
  INDEX `fk_book_client_idx` (`client_id` ASC) VISIBLE,
  CONSTRAINT `fk_book_plane`
    FOREIGN KEY (`plane_id`)
    REFERENCES `airport`.`plane` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_book_client`
    FOREIGN KEY (`client_id`)
    REFERENCES `airport`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Table `airport`.`client`
-- -----------------------------------------------------
INSERT INTO `airport`.`client` (`id`, `name`) VALUES ('1', 'Volodya');
INSERT INTO `airport`.`client` (`id`, `name`) VALUES ('2', 'Andriy');
INSERT INTO `airport`.`client` (`id`, `name`) VALUES ('3', 'Mykhaylo');
INSERT INTO `airport`.`client` (`id`, `name`) VALUES ('4', 'Aria');
INSERT INTO `airport`.`client` (`id`, `name`) VALUES ('5', 'Walker');

-- -----------------------------------------------------
-- Table `airport`.`plane_type`
-- -----------------------------------------------------
INSERT INTO `airport`.`plane_type` (`id`, `name`) VALUES ('1', 'Passenger');
INSERT INTO `airport`.`plane_type` (`id`, `name`) VALUES ('2', 'Army');
INSERT INTO `airport`.`plane_type` (`id`, `name`) VALUES ('3', 'Vehicle');
INSERT INTO `airport`.`plane_type` (`id`, `name`) VALUES ('4', 'Helicopter');

-- -----------------------------------------------------
-- Table `airport`.`book`
-- -----------------------------------------------------
INSERT INTO `airport`.`book` (`plane_id`, `client_id`) VALUES ('1', '1');
INSERT INTO `airport`.`book` (`plane_id`, `client_id`) VALUES ('1', '2');
INSERT INTO `airport`.`book` (`plane_id`, `client_id`) VALUES ('2', '4');
INSERT INTO `airport`.`book` (`plane_id`, `client_id`) VALUES ('3', '3');
INSERT INTO `airport`.`book` (`plane_id`, `client_id`) VALUES ('4', '5');

-- -----------------------------------------------------
-- Table `airport`.`plane`
-- -----------------------------------------------------
INSERT INTO `airport`.`plane` (`id`, `name`, `chair_amount`, `plane_type_id`) VALUES ('1', 'AN-230', '190', '1');
INSERT INTO `airport`.`plane` (`id`, `name`, `chair_amount`, `plane_type_id`) VALUES ('2', 'TU-359', '25', '2');
INSERT INTO `airport`.`plane` (`id`, `name`, `chair_amount`, `plane_type_id`) VALUES ('3', 'Boeing-777', '10', '3');
INSERT INTO `airport`.`plane` (`id`, `name`, `chair_amount`, `plane_type_id`) VALUES ('4', 'SJ-09', '250', '1');
INSERT INTO `airport`.`plane` (`id`, `name`, `chair_amount`, `plane_type_id`) VALUES ('5', 'DA-50', '3', '4');