package com.ostapiuk.DAO;

import com.ostapiuk.model.PlaneTypeEntity;

import java.sql.SQLException;

public interface PlaneTypeDAO extends GeneralDAO<PlaneTypeEntity, Integer> {
    PlaneTypeEntity findByName(String name) throws SQLException;
}
