package com.ostapiuk.DAO.implementation;

import com.ostapiuk.DAO.ClientDAO;
import com.ostapiuk.model.ClientEntity;
import com.ostapiuk.persistent.ConnectionManager;
import com.ostapiuk.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDAOImpl implements ClientDAO {

    private static final String GET_ALL = "select * from client";
    private static final String GET_BY_ID = "select * from client where id=?";
    private static final String GET_BY_NAME = "select * from client where name=?";
    private static final String INSERT = "insert into client (name) values (?)";
    private static final String UPDATE = "update client set name=? where id=?";
    private static final String DELETE = "delete from client where id=?";

    @Override
    public ClientEntity findByName(String name) throws SQLException {
        ClientEntity userEntity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_NAME)) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    userEntity = (ClientEntity) new Transformer(ClientEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return userEntity;
    }

    @Override
    public List<ClientEntity> findAll() throws SQLException {
        List<ClientEntity> userEntities = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
                while (resultSet.next()) {
                    userEntities.add((ClientEntity) new Transformer(ClientEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return userEntities;
    }

    @Override
    public ClientEntity findById(Integer id) throws SQLException {
        ClientEntity userEntity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    userEntity = (ClientEntity) new Transformer(ClientEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return userEntity;
    }

    @Override
    public int create(ClientEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, entity.getName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(ClientEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
