package com.ostapiuk.DAO.implementation;

import com.ostapiuk.DAO.PlaneTypeDAO;
import com.ostapiuk.model.PlaneTypeEntity;
import com.ostapiuk.persistent.ConnectionManager;
import com.ostapiuk.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlaneTypeDAOImpl implements PlaneTypeDAO {

    private static final String GET_ALL = "select * from plane_type";
    private static final String GET_BY_ID = "select * from plane_type where id=?";
    private static final String GET_BY_MODEL = "select * from plane_type where name=?";
    private static final String INSERT = "insert into plane_type (name) values (?)";
    private static final String UPDATE = "update plane_type set name=? where id=?";
    private static final String DELETE = "delete from plane_type where id=?";


    public PlaneTypeEntity findByName(String name) throws SQLException {
        PlaneTypeEntity laptopEntity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_MODEL)) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    laptopEntity = (PlaneTypeEntity) new Transformer(PlaneTypeEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return laptopEntity;
    }

    @Override
    public List<PlaneTypeEntity> findAll() throws SQLException {
        List<PlaneTypeEntity> laptopEntities = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
                while (resultSet.next()) {
                    laptopEntities.add((PlaneTypeEntity) new Transformer(PlaneTypeEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return laptopEntities;
    }

    @Override
    public PlaneTypeEntity findById(Integer id) throws SQLException {
        PlaneTypeEntity laptopEntity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    laptopEntity = (PlaneTypeEntity) new Transformer(PlaneTypeEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return laptopEntity;
    }

    @Override
    public int create(PlaneTypeEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, entity.getName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(PlaneTypeEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
