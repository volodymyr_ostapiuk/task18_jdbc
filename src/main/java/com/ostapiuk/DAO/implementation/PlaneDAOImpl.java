package com.ostapiuk.DAO.implementation;

import com.ostapiuk.DAO.PlaneDAO;
import com.ostapiuk.model.PlaneEntity;
import com.ostapiuk.persistent.ConnectionManager;
import com.ostapiuk.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlaneDAOImpl implements PlaneDAO {
    private static final String GET_ALL = "select * from plane";
    private static final String GET_BY_ID = "select * from plane where id=?";
    private static final String INSERT = "insert into plane (name, chair_amount, plane_type_id) values (?,?,?)";
    private static final String UPDATE = "update plane set login=?, password=?, laptop_id=? where id=?";
    private static final String DELETE = "delete from plane where id=?";

    @Override
    public List<PlaneEntity> findAll() throws SQLException {
        List<PlaneEntity> accounts = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
                while (resultSet.next()) {
                    accounts.add((PlaneEntity) new Transformer(PlaneEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return accounts;
    }

    @Override
    public PlaneEntity findById(Integer id) throws SQLException {
        PlaneEntity accountEntity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    accountEntity = (PlaneEntity) new Transformer(PlaneEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return accountEntity;
    }

    @Override
    public int create(PlaneEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getChairAmount());
            preparedStatement.setInt(3, entity.getLaptopID());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(PlaneEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getChairAmount());
            preparedStatement.setInt(3, entity.getLaptopID());
            preparedStatement.setInt(4, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
