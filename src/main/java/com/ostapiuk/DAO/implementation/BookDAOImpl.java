package com.ostapiuk.DAO.implementation;

import com.ostapiuk.DAO.BookDAO;
import com.ostapiuk.model.BookEntity;
import com.ostapiuk.model.ClientEntity;
import com.ostapiuk.model.PK_Book;
import com.ostapiuk.model.PlaneEntity;
import com.ostapiuk.persistent.ConnectionManager;
import com.ostapiuk.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDAOImpl implements BookDAO {

    private static final String GET_ALL_LAPTOPS = "select id, name from plane a join book b on a.id = b.client_id where plane_id=?";
    private static final String GET_ALL_USERS = "select id, name from client a join book b on a.id = b.plane_id where client_id=?";
    private static final String GET_ALL = "select * from book";
    private static final String INSERT = "insert into book (plane_id, client_id) values (?,?)";
    private static final String DELETE = "delete from book where plane_id=? and client_id=?";

    @Override
    public List<PlaneEntity> findByPlaneId(Integer plane_id)
            throws SQLException {
        List<PlaneEntity> list = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_LAPTOPS)) {
            preparedStatement.setInt(1, plane_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    list.add((PlaneEntity) new Transformer(PlaneEntity.class).fromResultSetToEntity(resultSet));
                }
                return list;
            }
        }
    }

    @Override
    public List<ClientEntity> findByClientId(Integer client_id)
            throws SQLException {
        List<ClientEntity> list = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USERS)) {
            preparedStatement.setInt(1, client_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    list.add((ClientEntity) new Transformer(ClientEntity.class).fromResultSetToEntity(resultSet));
                }
                return list;
            }
        }
    }

    @Override
    public List<BookEntity> findAll() throws SQLException {
        List<BookEntity> list = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
                while (resultSet.next()) {
                    list.add((BookEntity) new Transformer(BookEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return list;
    }

    @Override
    public BookEntity findById(PK_Book pk_book)
            throws SQLException {
        return null;
    }

    @Override
    public int create(BookEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setInt(1, entity.getPk().getPlaneID());
            preparedStatement.setInt(2, entity.getPk().getClientID());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(BookEntity entity) throws SQLException {
        return 0;
    }

    @Override
    public int delete(PK_Book pk_book) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, pk_book.getPlaneID());
            preparedStatement.setInt(2, pk_book.getClientID());
            return preparedStatement.executeUpdate();
        }
    }
}
