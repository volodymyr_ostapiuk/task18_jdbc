package com.ostapiuk.DAO;

import com.ostapiuk.model.PlaneEntity;

public interface PlaneDAO extends GeneralDAO<PlaneEntity, Integer> {

}
