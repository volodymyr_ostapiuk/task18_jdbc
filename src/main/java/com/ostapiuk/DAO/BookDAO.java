package com.ostapiuk.DAO;

import com.ostapiuk.model.BookEntity;
import com.ostapiuk.model.ClientEntity;
import com.ostapiuk.model.PK_Book;
import com.ostapiuk.model.PlaneEntity;

import java.sql.SQLException;
import java.util.List;

public interface BookDAO extends GeneralDAO<BookEntity, PK_Book> {
    List<PlaneEntity> findByPlaneId(Integer plane_id) throws SQLException;

    List<ClientEntity> findByClientId(Integer client_id) throws SQLException;
}
