package com.ostapiuk.DAO;

import com.ostapiuk.model.ClientEntity;

import java.sql.SQLException;

public interface ClientDAO extends GeneralDAO<ClientEntity, Integer> {
    ClientEntity findByName(String name) throws SQLException;
}
