package com.ostapiuk.ViewAndController;

import com.ostapiuk.model.*;
import com.ostapiuk.model.metadata.TableMetaData;
import com.ostapiuk.persistent.ConnectionManager;
import com.ostapiuk.service.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;
    private static Scanner input = new Scanner(System.in);
    private static Scanner input1 = new Scanner(System.in);

    public Menu() {
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        menu.put("A", "A  - Select all tables");
        menu.put("B", "B  - Select structure of DB");
        menu.put("C", "C  - Select tables names");

        menu.put("1", "1  - Table: Plane_type");
        menu.put("11", "11 - Create for Plane_type");
        menu.put("12", "12 - Update Plane_type");
        menu.put("13", "13 - Delete from Plane_type");
        menu.put("14", "14 - Select Plane_type");
        menu.put("15", "15 - Find Plane_type by ID");
        menu.put("16", "16 - Find Plane_type by Name");

        menu.put("2", "2  - Table: Plane");
        menu.put("21", "21 - Create for Plane");
        menu.put("22", "22 - Update Plane");
        menu.put("23", "23 - Delete from Plane");
        menu.put("24", "24 - Select Plane");
        menu.put("25", "25 - Find Plane by ID");

        menu.put("3", "3  - Table: ClientEntity");
        menu.put("31", "31 - Create for ClientEntity");
        menu.put("32", "32 - Update ClientEntity");
        menu.put("33", "33 - Delete from ClientEntity");
        menu.put("34", "34 - Select ClientEntity");
        menu.put("35", "35 - Find ClientEntity by ID");
        menu.put("36", "36 - Find ClientEntity by Name");

        menu.put("4", "4  - Table: Book");
        menu.put("41", "41 - Create for Book");
        menu.put("43", "42- Delete from Book");
        menu.put("44", "43 - Select Book");
        menu.put("45", "44 - Find all Planes");
        menu.put("46", "45 - Find all Clients");

        menu.put("Q", "Q  - exit");

        menuMethods.put("A", this::selectAllTables);
        menuMethods.put("B", this::takeStructureOfDB);
        menuMethods.put("C", this::selectAllTableNames);

        menuMethods.put("11", this::createPlaneType);
        menuMethods.put("12", this::updatePlaneType);
        menuMethods.put("13", this::deletePlaneType);
        menuMethods.put("14", this::selectAllPlaneTypes);
        menuMethods.put("15", this::findPlaneTypeByID);
        menuMethods.put("16", this::findPlaneTypeByName);

        menuMethods.put("21", this::createPlane);
        menuMethods.put("22", this::updatePlane);
        menuMethods.put("23", this::deletePlane);
        menuMethods.put("24", this::selectAllPlanes);
        menuMethods.put("25", this::findPlaneByID);

        menuMethods.put("31", this::createUser);
        menuMethods.put("32", this::updateUser);
        menuMethods.put("33", this::deleteUser);
        menuMethods.put("34", this::selectAllUsers);
        menuMethods.put("35", this::findUserByID);
        menuMethods.put("36", this::findUserByName);

        menuMethods.put("41", this::createBook);
        menuMethods.put("42", this::deleteBook);
        menuMethods.put("43", this::selectAllBook);
        menuMethods.put("44", this::findAllPlanes);
        menuMethods.put("45", this::findAllClients);
    }

    private void takeStructureOfDB() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaDataService metaDataService = new MetaDataService();
        List<TableMetaData> structure = metaDataService.getTablesStructure();
        System.out.println("--------Tables: " + connection.getCatalog() + " --------");
        for (TableMetaData tables : structure) {
            System.out.println(tables);
        }
    }

    private void selectAllTableNames() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaDataService metaDataService = new MetaDataService();
        List<String> structure = metaDataService.findAllTableName();
        System.out.println("--------Tables names: " + connection.getCatalog() + " --------");
        for (String tables : structure) {
            System.out.println(tables);
        }
    }

    private void selectAllTables() throws SQLException {
        selectAllPlaneTypes();
        selectAllPlanes();
        selectAllUsers();
        selectAllBook();
    }

    //--------------------PlaneType
    private void findPlaneTypeByName() throws SQLException {
        System.out.println("Input name for PlaneType: ");
        String name = input.nextLine();
        PlaneTypeService service = new PlaneTypeService();
        PlaneTypeEntity entity = service.findByName(name);
        System.out.println(entity);
    }

    private void selectAllPlaneTypes() throws SQLException {
        System.out.println("\nTable: PlaneType");
        PlaneTypeService service = new PlaneTypeService();
        List<PlaneTypeEntity> entities = service.findAll();
        for (PlaneTypeEntity entity : entities) {
            System.out.println(entity);
        }
    }

    private void findPlaneTypeByID() throws SQLException {
        System.out.println("Input id for PlaneType: ");
        Integer id = input.nextInt();
        PlaneTypeService service = new PlaneTypeService();
        PlaneTypeEntity entity = service.findById(id);
        System.out.println(entity);
    }

    private void createPlaneType() throws SQLException {
        System.out.println("Input id for PlaneType: ");
        Integer id = input.nextInt();
        System.out.println("Input name for PlaneType: ");
        String name = input1.nextLine();
        PlaneTypeEntity entity = new PlaneTypeEntity(id, name);
        PlaneTypeService service = new PlaneTypeService();
        int count = service.create(entity);
        System.out.printf("%d row(-s) created", count);
    }

    private void updatePlaneType() throws SQLException {
        System.out.println("Input id for PlaneType: ");
        Integer id = input.nextInt();
        System.out.println("Input name for PlaneType: ");
        String name = input1.nextLine();
        PlaneTypeEntity entity = new PlaneTypeEntity(id, name);
        PlaneTypeService service = new PlaneTypeService();
        int count = service.update(entity);
        System.out.printf("%d row(-s) updated", count);
    }

    private void deletePlaneType() throws SQLException {
        System.out.println("Input id for PlaneType: ");
        Integer id = input.nextInt();
        PlaneTypeService service = new PlaneTypeService();
        int count = service.delete(id);
        System.out.printf("%d row(-s) deleted", count);
    }

    //--------------------Plane
    private void selectAllPlanes() throws SQLException {
        System.out.println("\nTable: Plane");
        PlaneService service = new PlaneService();
        List<PlaneEntity> entities = service.findAll();
        for (PlaneEntity entity : entities) {
            System.out.println(entity);
        }
    }

    private void findPlaneByID() throws SQLException {
        System.out.println("Input id for Plane: ");
        Integer id = input.nextInt();
        PlaneService service = new PlaneService();
        PlaneEntity entity = service.findById(id);
        System.out.println(entity);
    }

    private void createPlane() throws SQLException {
        System.out.println("Input id for Plane: ");
        Integer id = input.nextInt();
        System.out.println("Input name for Plane: ");
        String name = input.nextLine();
        System.out.println("Input amount for Plane: ");
        Integer amount = input.nextInt();
        System.out.println("Input plane_type_id for Plane: ");
        Integer plane_type_id = input.nextInt();
        PlaneEntity entity = new PlaneEntity(id, name, amount, plane_type_id);
        PlaneService service = new PlaneService();
        int count = service.create(entity);
        System.out.printf("%d row(-s) created", count);
    }

    private void updatePlane() throws SQLException {
        System.out.println("Input id for Plane: ");
        Integer id = input.nextInt();
        System.out.println("Input name for Plane: ");
        String name = input.nextLine();
        System.out.println("Input amount for Plane: ");
        Integer amount = input.nextInt();
        System.out.println("Input plane_type_id for Plane: ");
        Integer plane_type_id = input.nextInt();
        PlaneEntity entity = new PlaneEntity(id, name, amount, plane_type_id);
        PlaneService service = new PlaneService();
        int count = service.update(entity);
        System.out.printf("%d row(-s) updated", count);
    }

    private void deletePlane() throws SQLException {
        System.out.println("Input id for Plane: ");
        Integer id = input.nextInt();
        PlaneService service = new PlaneService();
        int count = service.delete(id);
        System.out.printf("%d row(-s) deleted", count);
    }

    //--------------------Client
    private void findUserByName() throws SQLException {
        System.out.println("Input name for Client: ");
        String name = input.nextLine();
        ClientService userService = new ClientService();
        ClientEntity userEntity = userService.findByName(name);
        System.out.println(userEntity);
    }

    private void selectAllUsers() throws SQLException {
        System.out.println("\nTable: Client");
        ClientService userService = new ClientService();
        List<ClientEntity> userEntities = userService.findAll();
        for (ClientEntity userEntity : userEntities) {
            System.out.println(userEntity);
        }
    }

    private void findUserByID() throws SQLException {
        System.out.println("Input id for Client: ");
        Integer id = input.nextInt();
        ClientService userService = new ClientService();
        ClientEntity userEntity = userService.findById(id);
        System.out.println(userEntity);
    }

    private void createUser() throws SQLException {
        System.out.println("Input id for Client: ");
        Integer id = input.nextInt();
        System.out.println("Input name for Client: ");
        String name = input.nextLine();
        ClientEntity userEntity = new ClientEntity(id, name);
        ClientService userService = new ClientService();
        int count = userService.create(userEntity);
        System.out.printf("%d row(-s) created", count);
    }

    private void updateUser() throws SQLException {
        System.out.println("Input id for Client: ");
        Integer id = input.nextInt();
        System.out.println("Input name for Client: ");
        String name = input.nextLine();
        ClientEntity userEntity = new ClientEntity(id, name);
        ClientService userService = new ClientService();
        int count = userService.update(userEntity);
        System.out.printf("%d row(-s) updated", count);
    }

    private void deleteUser() throws SQLException {
        System.out.println("Input id for Client: ");
        Integer id = input.nextInt();
        ClientService userService = new ClientService();
        int count = userService.delete(id);
        System.out.printf("%d row(-s) deleted", count);
    }

    //--------------------Book
    private void selectAllBook() throws SQLException {
        System.out.println("\n Table Book: ");
        BookService service = new BookService();
        List<BookEntity> entities = service.findAll();
        for (BookEntity entity : entities) {
            System.out.println(entity);
        }
    }

    private void createBook() throws SQLException {
        System.out.println("Input plane_id for Book: ");
        Integer planeID = input.nextInt();
        System.out.println("Input client_id for Book: ");
        Integer clientID = input.nextInt();
        BookEntity entity = new BookEntity(new PK_Book(planeID, clientID));
        BookService service = new BookService();
        int count = service.create(entity);
        System.out.printf("%d row(-s) created", count);
    }

    private void findAllPlanes() throws SQLException {
        System.out.println("Input plane_id to find Planes: ");
        Integer id = input.nextInt();
        BookService service = new BookService();
        List<PlaneEntity> all = service.findByPlaneID(id);
        for (PlaneEntity entity : all) {
            System.out.println(entity);
        }
    }

    private void findAllClients() throws SQLException {
        System.out.println("Input client_id to find Clients: ");
        Integer id = input.nextInt();
        BookService service = new BookService();
        List<ClientEntity> all = service.findByClientID(id);
        for (ClientEntity entity : all) {
            System.out.println(entity);
        }
    }

    private void deleteBook() throws SQLException {
        System.out.println("Input plane_id for Book: ");
        Integer plane_id = input.nextInt();
        System.out.println("Input client_id for Book: ");
        Integer client_id = input.nextInt();
        PK_Book pk = new PK_Book(plane_id, client_id);
        BookService service = new BookService();
        int count = service.delete(pk);
        System.out.printf("There are deleted %d rows\n", count);
    }

    //--------------------Menu
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    private void outputSubMenu(String fig) {
        System.out.println("\nSubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine().toUpperCase();
            }
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
