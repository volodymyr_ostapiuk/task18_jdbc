package com.ostapiuk.service;

import com.ostapiuk.DAO.implementation.MetaDataDAOImpl;
import com.ostapiuk.model.metadata.TableMetaData;

import java.sql.SQLException;
import java.util.List;

public class MetaDataService {

    public List<String> findAllTableName() throws SQLException {
        return new MetaDataDAOImpl().findAllTableName();
    }

    public List<TableMetaData> getTablesStructure() throws SQLException {
        return new MetaDataDAOImpl().getTablesStructure();
    }
}
