package com.ostapiuk.service;

import com.ostapiuk.DAO.implementation.PlaneTypeDAOImpl;
import com.ostapiuk.model.PlaneTypeEntity;

import java.sql.SQLException;
import java.util.List;

public class PlaneTypeService {
    public PlaneTypeEntity findByName(String name) throws SQLException {
        return new PlaneTypeDAOImpl().findByName(name);
    }

    public List<PlaneTypeEntity> findAll() throws SQLException {
        return new PlaneTypeDAOImpl().findAll();
    }

    public PlaneTypeEntity findById(Integer id) throws SQLException {
        return new PlaneTypeDAOImpl().findById(id);
    }

    public int create(PlaneTypeEntity entity) throws SQLException {
        return new PlaneTypeDAOImpl().create(entity);
    }

    public int update(PlaneTypeEntity entity) throws SQLException {
        return new PlaneTypeDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new PlaneTypeDAOImpl().delete(id);
    }
}
