package com.ostapiuk.service;

import com.ostapiuk.DAO.implementation.BookDAOImpl;
import com.ostapiuk.model.BookEntity;
import com.ostapiuk.model.ClientEntity;
import com.ostapiuk.model.PK_Book;
import com.ostapiuk.model.PlaneEntity;

import java.sql.SQLException;
import java.util.List;

public class BookService {

    public List<PlaneEntity> findByPlaneID(Integer id) throws SQLException {
        return new BookDAOImpl().findByPlaneId(id);
    }

    public List<ClientEntity> findByClientID(Integer id) throws SQLException {
        return new BookDAOImpl().findByClientId(id);
    }

    public List<BookEntity> findAll() throws SQLException {
        return new BookDAOImpl().findAll();
    }

    public int create(BookEntity book) throws SQLException {
        return new BookDAOImpl().create(book);
    }

    public int delete(PK_Book pk) throws SQLException {
        return new BookDAOImpl().delete(pk);
    }
}
