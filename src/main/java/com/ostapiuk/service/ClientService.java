package com.ostapiuk.service;

import com.ostapiuk.DAO.implementation.ClientDAOImpl;
import com.ostapiuk.model.ClientEntity;

import java.sql.SQLException;
import java.util.List;

public class ClientService {

    public ClientEntity findByName(String name) throws SQLException {
        return new ClientDAOImpl().findByName(name);
    }

    public List<ClientEntity> findAll() throws SQLException {
        return new ClientDAOImpl().findAll();
    }

    public ClientEntity findById(Integer id) throws SQLException {
        return new ClientDAOImpl().findById(id);
    }

    public int create(ClientEntity entity) throws SQLException {
        return new ClientDAOImpl().create(entity);
    }

    public int update(ClientEntity entity) throws SQLException {
        return new ClientDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new ClientDAOImpl().delete(id);
    }
}
