package com.ostapiuk.service;

import com.ostapiuk.DAO.implementation.PlaneDAOImpl;
import com.ostapiuk.model.PlaneEntity;

import java.sql.SQLException;
import java.util.List;

public class PlaneService {

    public List<PlaneEntity> findAll() throws SQLException {
        return new PlaneDAOImpl().findAll();
    }

    public PlaneEntity findById(Integer id) throws SQLException {
        return new PlaneDAOImpl().findById(id);
    }

    public int create(PlaneEntity entity) throws SQLException {
        return new PlaneDAOImpl().create(entity);
    }

    public int update(PlaneEntity entity) throws SQLException {
        return new PlaneDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new PlaneDAOImpl().delete(id);
    }
}
