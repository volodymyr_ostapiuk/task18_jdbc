package com.ostapiuk.model;

import com.ostapiuk.model.annotation.PrimaryKeyComposite;
import com.ostapiuk.model.annotation.Table;

@Table(name = "book")
public class BookEntity {

    @PrimaryKeyComposite
    private PK_Book pk;

    public BookEntity() {
    }

    public BookEntity(PK_Book pk) {
        this.pk = pk;
    }

    public PK_Book getPk() {
        return pk;
    }

    public void setPk(PK_Book pk) {
        this.pk = pk;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-5d", pk.getPlaneID(), pk.getClientID());
    }
}
