package com.ostapiuk.model;

import com.ostapiuk.model.annotation.Column;
import com.ostapiuk.model.annotation.PrimaryKey;
import com.ostapiuk.model.annotation.Table;

@Table(name = "account")
public class PlaneEntity {

    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "chair_amount")
    private Integer chairAmount;
    @Column(name = "laptop_id")
    private Integer laptopID;

    public PlaneEntity() {
    }

    public PlaneEntity(Integer id, String login, Integer password,
                       Integer laptopID) {
        this.id = id;
        this.name = login;
        this.chairAmount = password;
        this.laptopID = laptopID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getChairAmount() {
        return chairAmount;
    }

    public void setChairAmount(Integer chairAmount) {
        this.chairAmount = chairAmount;
    }

    public Integer getLaptopID() {
        return laptopID;
    }

    public void setLaptopID(Integer laptopID) {
        this.laptopID = laptopID;
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-15s %s", id, name, chairAmount, laptopID);
    }
}
