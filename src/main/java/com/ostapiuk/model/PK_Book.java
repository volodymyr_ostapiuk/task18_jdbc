package com.ostapiuk.model;

import com.ostapiuk.model.annotation.Column;

public class PK_Book {
    @Column(name = "plane_id")
    private Integer planeID;
    @Column(name = "client_id")
    private Integer clientID;

    public PK_Book() {
    }

    public PK_Book(Integer laptopID, Integer userID) {
        this.planeID = laptopID;
        this.clientID = userID;
    }

    public Integer getPlaneID() {
        return planeID;
    }

    public void setPlaneID(Integer planeID) {
        this.planeID = planeID;
    }

    public Integer getClientID() {
        return clientID;
    }

    public void setClientID(Integer clientID) {
        this.clientID = clientID;
    }
}
