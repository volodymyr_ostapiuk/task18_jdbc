package com.ostapiuk.model;

import com.ostapiuk.model.annotation.Column;
import com.ostapiuk.model.annotation.PrimaryKey;
import com.ostapiuk.model.annotation.Table;

@Table(name = "client")
public class ClientEntity {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;

    public ClientEntity() {
    }

    public ClientEntity(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-5s %s", id, name);
    }
}
